const express = require('express')
const connectDB = require('./config/db')
const dotenv = require('dotenv')
const morgan = require('morgan')

const errorHandler = require('./middlewares/errorHandler.middleware')
const employee = require('./routers/employess.route')

const env = dotenv.config()
if (env.error) {
  throw env.error
}

connectDB()

const app = express()

app.use(express.json())

// dev logging middlware
if (process.env.NODE_ENV === 'development') app.use(morgan('dev'))

// mount routers
app.use('/api/v1/employee', employee)

app.use(errorHandler)

const PORT = process.env.PORT || 5000

const server = app.listen(
  PORT,
  console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`)
)

// handle unhandled promise rejections
process.on('unhandledRejection', (err, promise) => {
  console.log(`Error: ${err.message}`.red)

  server.close(() => process.exit(1))
})
