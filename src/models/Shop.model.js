const mongoose = require('mongoose')

const ShopSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Must add name']
  },
  address: {
    type: String,
    required: [true, 'Must add address']
  },
  telephone: {
    type: String,
    required: [true, 'Must add telephone'],
    maxlength: [20, 'Must be less then 20 characters']
  },
  employeeId: {
    type: mongoose.Schema.ObjectId,
    ref: 'Employee',
    required: true
  }
})

module.exports = mongoose.model('Shop', ShopSchema)
