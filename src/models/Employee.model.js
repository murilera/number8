const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/number8')
require('./EmployeeType.model')
require('./Shop.model')

const EmployeeSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, 'Must add name'],
      trim: true
    },
    telephone: {
      type: String,
      required: [true, 'Must add telephone'],
      maxlength: [20, 'Must be less then 20 characters']
    },
    address: {
      type: String,
      required: [true, 'Must add address']
    },
    employeeDate: {
      type: Date,
      default: Date.now
    }
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
  }
)

EmployeeSchema.virtual('type', {
  ref: 'EmployeeType',
  localField: '_id',
  foreignField: 'employeeId',
  justOne: false
})

EmployeeSchema.virtual('shop', {
  ref: 'Shop',
  localField: '_id',
  foreignField: 'employeeId',
  justOne: false
})

module.exports = mongoose.model('Employee', EmployeeSchema)
