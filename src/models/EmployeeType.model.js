const mongoose = require('mongoose')

const EmployeeTypeSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Must add name']
  },
  salary: {
    type: Number,
    required: [true, 'Must add salary']
  },
  employeeId: {
    type: mongoose.Schema.ObjectId,
    ref: 'Employee',
    required: true
  }
})

module.exports = mongoose.model('EmployeeType', EmployeeTypeSchema)
