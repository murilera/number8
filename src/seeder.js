const fs = require('fs')
const path = require('path')
const mongoose = require('mongoose')

const Employee = require('./models/Employee.model')
const EmployeeType = require('./models/EmployeeType.model')
const Shop = require('./models/Shop.model')

const mongoURI = 'mongodb://localhost:27017/number8'
mongoose.connect(mongoURI, {
  useNewUrlParser: true,
  useUnifiedTopology: true
})

const employeesPath = path.join(__dirname, '/_data/employee.json')
const employees = JSON.parse(fs.readFileSync(employeesPath, 'utf-8'))
const employeeTypes = JSON.parse(
  fs.readFileSync(path.join(__dirname, '/_data/employeeType.json'), 'utf-8')
)
const shops = JSON.parse(
  fs.readFileSync(path.join(__dirname, '/_data/shop.json'), 'utf-8')
)

console.log(employees)

const seedData = async () => {
  try {
    const employeeSeed = await Employee.create(employees)
    const employeeTypeSeed = await EmployeeType.create(employeeTypes)
    const shopsSeed = await Shop.create(shops)

    console.log(employeeSeed, employeeTypeSeed, shopsSeed)
    process.exit()
  } catch (err) {
    console.error(err)
  }
}

const deleteData = async () => {
  try {
    const employeeRemove = await Employee.remove()
    const employeeTypeRemove = await EmployeeType.remove()
    const shopsRemove = await Shop.remove()

    console.log(employeeRemove, employeeTypeRemove, shopsRemove)
    process.exit(1)
  } catch (err) {
    console.error(err)
  }
}

if (process.argv[2] == '-i') {
  seedData()
}

if (process.argv[2] == '-d') {
  deleteData()
}
