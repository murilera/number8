const mongoose = require('mongoose')

const mongoURI = 'mongodb://localhost:27017/number8'

const connectDB = async () => {
  const conn = await mongoose.connect(mongoURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })

  console.log(`MongoDB Connect: ${conn.connection.host}`)
}

module.exports = connectDB
