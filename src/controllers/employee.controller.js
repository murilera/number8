const path = require('path')
const ErrorResponse = require('../utils/errorResponse')
const Employee = require('../models/Employee.model')
const asyncHandler = require('../middlewares/asyncHandler.middleware')

// @desc    Get all Employees
// @route   GET /api/v1/employee
// @access  Public
exports.getAllEmployees = asyncHandler(async (req, res, next) => {
  const employees = await Employee.find().populate(['type', 'shop'])

  res.status(200).json({
    success: true,
    data: employees
  })
})

// @desc    Get single employee
// @route   GET /api/v1/employee/:id
// @access  Public
exports.getEmployeeById = asyncHandler(async (req, res, next) => {
  try {
    const employee = await Employee.findById(req.params.id)

    if (!employee) {
      return next(
        new ErrorResponse(`Employee not found with id of ${req.params.id}`, 404)
      )
    }

    res.status(200).json({
      success: true,
      data: employee
    })
  } catch (err) {
    next(err)
  }
})

// @desc    Create new employee
// @route   POST /api/v1/employee/
// @access  Private
exports.createEmployee = asyncHandler(async (req, res, next) => {
  const employee = await Employee.create(req.body)

  res.status(201).json({
    success: true,
    msg: 'create new employee',
    data: employee
  })
})

// @desc    Update employee
// @route   PUT /api/v1/employee/:id
// @access  Private
exports.updateEmployee = asyncHandler(async (req, res, next) => {
  const employee = await Employee.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true
  })
  if (!employee) {
    return next(
      new ErrorResponse(`Employee not found with id of ${req.params.id}`, 404)
    )
  }

  res.status(200).json({
    success: true,
    data: employee
  })
})

// @desc    Delete employee
// @route   DELETE /api/v1/employee/:id
// @access  Private
exports.deleteEmployee = asyncHandler(async (req, res, next) => {
  const employee = await Employee.findById(req.params.id)
  if (!employee) {
    return next(
      new ErrorResponse(`Employee not found with id of ${req.params.id}`, 404)
    )
  }

  Employee.remove()

  res.status(200).json({
    success: true,
    data: {}
  })
})
